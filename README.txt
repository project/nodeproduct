Description
------------------
This module should be used within ecommerce package and depends on product.module.

It allows to assign a certain product type to any content type declared in the system (CCK types as well).
After that all nodes of this type will be created as products without a need to add them to a store manually.

Configuring
-----------

1. Visit admin/content/types page and choose any content type, click edit.
"Workflow" fieldset will have "Product type" combobox where you specify which product type this content
should inherit automatically when adding it.

2. Go to admin/user/access page and enable "assign product type" permission for all roles you want to
be able to create such products.

Author
-----------------
ARDAS group <www.ardas.dp.ua>